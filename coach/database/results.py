import sqlite3


conn = sqlite3.connect("results.db")  # или :memory: чтобы сохранить в RAM
cursor = conn.cursor()

# Создание таблицы
cursor.execute("""CREATE TABLE IF NOT EXISTS results
                  (layers text, optimizer text, epochs int,
                  train_accuracy real, train_loss real,
                  test_accuracy real, test_loss real,
                   train_set_len int, test_set_len int,
                   categories text)
               """)

entry = {}

def verify_entry():
    global entry
    return ["layers","optimizer","epochs", "train_accuracy", "train_loss", "test_accuracy", "test_loss", "train_set_len", "test_set_len", "categories"] in entry

def add_parameters(layers):
    global entry
    entry["layers"] = layers

def add_optimizer(layers):
    global entry
    entry["layers"] = layers

def add_epochs(layers):
    global entry
    entry["layers"] = layers

def insert(layers, optimizer):
    # Вставляем данные в таблицу
    cursor.execute("""INSERT INTO results
                      VALUES ("Glow", "Andy Hunter", "7/24/2012",
                      "Xplore Records", "MP3")"""
                   )

    # Сохраняем изменения
    conn.commit()

