import ui.connector as connector
import argparse
from sys import exit

def main():
    parser = create_parser()
    args = parser.parse_args()

    if not vars(args):
        parser.print_usage()
    else:
        if args.crawl:
            if not (args.categories_filename and args.image_folder):
                parser.print_usage()
                exit(1)
            connector.crawl(args.categories_filename, args.image_folder, args.image_size, args.max_results)

        elif args.train:

            if not args.test and not args.image_folder:
                parser.print_usage()
                exit(1)

            connector.train(int(args.epochs), args.image_folder, args.image_size, args.config_file, args.input_model, args.test, args.models_folder)

        elif args.predict:
            if not args.image_folder or not args.input_model:
                parser.print_usage()
                exit(1)

            connector.predict(args.input_model, args.image_folder)

        else:
            parser.print_usage()
            print("No action selected. Exiting...")

def create_parser():
    parser = argparse.ArgumentParser(description="Neural Network trainee")

    # SHARED arguments
    parser.add_argument("-iS",
                            action="store",
                            type=int,
                            dest="image_size",
                            default=64,
                            help=" [X] pixels of X*X image \
                            \nset processed image size")
    parser.add_argument("-iF",
                            action="store",
                            type=str,
                            dest="image_folder",
                            default=None,
                            help="folder with struct: \
                            \nimage_folder\n\t<-cat1\n\t\t<--training\n\t\t<--test\n\t\n\t\t<-...")


    # WEB SCRAPPER arguments
    parser.add_argument("-c",
                            action="store_const",
                            dest="crawl",
                            const=True,
                            default=False,
                            help=" search for images in net and make dataset\
                            \n! saves images in image_folder ")
    parser.add_argument("-cF",
                            action="store",
                            type=str,
                            dest="categories_filename",
                            help=".json file with struct: \
                            \n{\n\t\"cat1\":[searchtext1,...],\n\t...\n}")

    parser.add_argument("-m",
                            action="store",
                            type=int,
                            dest="max_results",
                            default=20,
                            help=" results output limit for one request ")

    # NN Classificator arguments
    parser.add_argument("-t",
                            action="store_const",
                            dest="train",
                            const=True,
                            default=False,
                            help=" train neural network with dataset in image_folder")
    parser.add_argument("--test",
                            action="store_const",
                            dest="test",
                            const=True,
                            default=False,
                            help=" train neural network with test data fashion_mnist")

    """
    LEGACY
    ______
    parser.add_argument("-T",
                        action="store",
                        dest="model_type",
                        type=str,
                        default="cnn",
                        help="specifies model type (mp, cnn)")
    """

    parser.add_argument("-mF",
                            action="store",
                            dest="models_folder",
                            type=str,
                            default="models",
                            help="specifies folder where to save trained models")

    parser.add_argument("-mC",action="store",
                            dest="config_file",
                            type=str,
                            default="cfg.json",
                            help="specifies model config with structure of neural network")

    parser.add_argument("-e",
                            action="store",
                            dest="epochs",
                            type=int,
                            default=10,
                            help="specifies number of epochs" +
                            "\f Default: 10")

    # Model prediction arguments
    parser.add_argument("-p",
                            action="store_const",
                            dest="predict",
                            const=True,
                            default=False,
                            help=" predict set with preexisted model")
    parser.add_argument("-iM",
                            action="store",
                            dest="input_model",
                            type=str,
                            default=None,
                            help="specifies model for prediction")



    return parser
