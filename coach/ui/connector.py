from intellect import trainer
from crawler import crawler
import filesystem
import os

def crawl(categories_filename, image_folder, image_size, max_results):
    categories_dict = filesystem.open_json(os.path.abspath(categories_filename))
    c = crawler.Crawler(categories_dict, os.path.abspath(image_folder), image_size, max_results)
    c.run()

def train(epochs, image_folder=None, image_size=None, parameters=None, model=None, test=False, models_folder=None):
    parameters = filesystem.parse_parameters(parameters)
    tr = trainer.Trainer(model=model, parameters=parameters, test=test)

    if not test: tr.set_dataset(dataset_folder=os.path.abspath(image_folder))

    tr.run(int(epochs))

    if models_folder:
        tr.save(os.path.join(models_folder, "trained_model"))


def predict(model, image_folder):
    pass
