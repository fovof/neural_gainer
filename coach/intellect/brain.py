from intellect.base import Classificator

from tensorflow import keras

default_parameters = {
    "layers":[
        {"layer_type":"Conv2D","nodes":32,"activation":"sigmoid","kernel":(2,2)},
        {"layer_type":"MaxPoling2D","kernel":(2,2)},
        {"layer_type":"Flatten"},
        {"layer_type":"Conv2d","nodes":64,"activation":"sigmoid","kernel":(2,2)},
        {"layer_type":"Dropout", "value":0.25},
        {"layer_type":"Dense", "nodes":128, "activation":"sigmoid"},
        {"layer_type":"Dropout", "value":0.5},
        {"layer_type":"Dense", "nodes": None, "activation": "softmax"}
    ],
    "optimizer":"adam",
    "loss":"sparse_categorical_crossentropy",
    "metrics":["accuracy"]
}



class FlexClassificator(Classificator):

    def _ver_flatten(self):
        return True
    def _ver_conv2d(self):
        return True
    def _ver_maxpoling2d(self):
        return True
    def _ver_dropout(self):
        return True
    def _ver_dense(self):
        return True

    def _lower_layer(self, layer):
        for param in layer.keys():
            if type(layer[param]) == str:
                layer[param] = layer[param].lower()


    def _layer_type_selection(self, layer):
        self._lower_layer(layer)
        if layer["layer_type"] == "flatten":
            if self._ver_flatten():
                layer = keras.layers.Flatten()

        elif layer["layer_type"] == "conv2d":
            if self._ver_conv2d():
                layer = keras.layers.Conv2D(layer["nodes"], layer["kernel"], activation=layer["activation"])


        elif layer["layer_type"] == "maxpoling2d":
            if self._ver_maxpoling2d():
                layer = keras.layers.MaxPooling2D(layer["kernel"])

        elif layer["layer_type"] == "dropout":
            if self._ver_dropout():
                layer = keras.layers.Dropout(layer["value"])

        elif layer["layer_type"] == "dense":
            if self._ver_dense():
                try:
                    if layer["nodes"]:
                        layer = keras.layers.Dense(layer["nodes"], activation=layer["activation"])
                    else:
                        layer = keras.layers.Dense(len(self.class_names), activation=layer["activation"])
                except KeyError:
                    layer = keras.layers.Dense(len(self.class_names), activation=layer["activation"])

        return layer

    def init_model(self, parameters=default_parameters):
        #parameters = default_parameters
        print(self.set_shape, self.class_names, "\n\n\n\n")

        self._validate_and_set_parameters(parameters)

        layers = []

        for layer in self.parameters["layers"]:
            layer = self._layer_type_selection(layer)
            layers.append(layer)

        self.model = keras.Sequential(layers)

        self.model.compile(optimizer=self.parameters["optimizer"],
              loss=self.parameters["loss"],
              metrics=self.parameters["metrics"])



    def train(self, train_set, train_labels, epochs):
        train_set = train_set.reshape(train_set.shape + (1,))
        train_labels = train_labels
        if not self.model:
            raise ValueError("Set or init model with init_model()")

        self._validate_and_set_fields(train_set, train_labels)

        self.model.fit(train_set, train_labels, epochs=epochs)


    def test(self, test_set,  test_labels):
        test_set = test_set.reshape(test_set.shape + (1,))
        if self.validate_set(test_set) and self._validate_labels_num(test_labels):
            test_loss, test_acc = self.model.evaluate(test_set,  test_labels, verbose=2)
        else:
            return False, False

        return test_loss, test_acc
