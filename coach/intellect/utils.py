import numpy as np
from crawler.formater import Formater
import random

def parse_dataset_struct(dataset_struct, image_size=64):
    class_names = list(dataset_struct.keys())
    train_set = []
    train_labels = []
    test_set = []
    test_labels = []

    train_sorting_list = []
    test_sorting_list = []

    f = Formater(image_size)

    for i in range(len(class_names)):
        for entry in dataset_struct[class_names[i]]["train"]:
            train_sorting_list.append([i,f.format_image(entry)])
        for entry in dataset_struct[class_names[i]]["test"]:
            test_sorting_list.append([i,f.format_image(entry)])

    random.shuffle(train_sorting_list)
    random.shuffle(test_sorting_list)

    for entry in train_sorting_list:
        train_labels.append(entry[0])
        train_set.append(np.asarray(entry[1]) / 255.0)

    for entry in test_sorting_list:
        test_labels.append(entry[0])
        test_set.append(np.asarray(entry[1]) / 255.0)



    return np.array(train_set), np.array(train_labels), np.array(test_set), np.array(test_labels), class_names
