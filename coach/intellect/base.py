import numpy as np

default_parameters = {
    "layers":[
        {"nodes":128,"activation":"sigmoid"},
        {"nodes":256,"activation":"relu"},
        {"nodes":512,"activation":"sigmoid"}
    ],
    "optimizer":"adam",
    "loss":"sparse_categorical_crossentropy"
}

class BaseModel(object):
    model = None
    set_shape = (0,)
    labels_shape = (0,)

    def __init__(self, model=None):
        if model:
            self.model = model

    def _deep_validate_set(self, set):
        set = np.array(set)
        #if len(set.shape) == 4:
        #    set = set.reshape(set.shape[:-1])

        return not (np.all(set >= 0) and np.all(set <= 1))

    def validate_set(self, set):
        print(np.shape(set))
        if len(np.shape(set)) < 2:
            return False

        if self._deep_validate_set(set):
            return False

        if self.set_shape == (0,):
            self.set_shape = np.shape(set)[1:]
        elif np.shape(set)[1:] != self.set_shape and np.shape(set)[1:-1] != self.set_shape:   #crunch
            print(np.shape(set)[1:-1], self.set_shape)
            return False

        if self.labels_shape == (0,):
            self.labels_shape = np.shape(set)[0:1]

        return True

    def train(self, train_set, train_labels, epochs):
        raise ImportError("Train function not set.")

    def test(self, test_set, test_labels):
        raise ImportError("Test function not set.")

    def predict(self, set):
        if self.validate_set(set):
            return self.model.predict(set)
        else:
            return False

    def save(self, set):
        self.model.save(set)


class Classificator(BaseModel):

    labels_num = 0
    class_names = []

    def set_class_names(self, class_names):
        self._set_class_names(class_names)

    def _validate_and_set_parameters(self, parameters):
        self.parameters = parameters

    def _set_class_names(self, class_names):

        if self.class_names == [x for x in range(self.labels_num)] or not self.class_names:

            self.class_names = class_names

    def _validate_and_set_fields(self, train_set, train_labels):

        if self.validate_set(train_set):
            self.train_set = np.array(train_set)
        else:
            raise ValueError("Train set has wrong format or shape.")

        if self._validate_labels_shape(train_labels):
            self.train_labels = np.array(train_labels)
        else:
            raise ValueError("Train labels have wrong shape. " +
                                "Got {0}, need {1}".format(
                                    str(np.shape(train_labels)),
                                    str(self.labels_shape)
                                    )
                                )

        self._validate_labels_num(train_labels)


    def _validate_labels_shape(self, labels):
        if np.shape(labels) != self.labels_shape:
            return False
        return True

    def _validate_labels_num(self, labels):
        if self.labels_num == 0:
            self.labels_num = np.max(labels) + 1 # assuming index starts with 0
        elif np.max(labels) + 1 != self.labels_num:
            raise ValueError("New labels dont match initial categories number. \
                                        Got {0}  and {1} categories.".format(
                                            np.max(labels) + 1,
                                            self.labels_num
                                            )
                                        )

        if not self.class_names:
            self.class_names = [x for x in range(self.labels_num)]
        else:
            if len(self.class_names) != self.labels_num:
                raise ValueError("Class names dont match labels number. \
                                    Got {0} classes fo {1} labels.".format(
                                        len(self.class_names),
                                        self.labels_num
                                        )
                                    )

        return True

    def vanish_shapes(self):
        self.set_shape = (0,)
        self.labels_num = 0

    def new_training_set(self, train_set, train_labels):
        self.labels_num = 0
        self._validate_and_set_fields(train_set, train_labels)
        self._validate_labels_num(train_labels)

