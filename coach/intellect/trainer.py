import tensorflow as tf
import matplotlib.pyplot as plt

from tensorflow import keras

import intellect.brain as brain
from intellect.utils import parse_dataset_struct

import filesystem as fs


class Trainer(object):

    mp_brain = None
    model = None

    def test(self):
        fashion_mnist = keras.datasets.fashion_mnist
        (self.train_set, self.train_labels), (self.test_set, self.test_labels) = fashion_mnist.load_data()

        self.train_set = self.train_set / 255.0
        self.test_set = self.test_set / 255.0

        self.class_names = ["T-shirt/top", "Trouser", "Pullover", "Dress", "Coat",
               "Sandal", "Shirt", "Sneaker", "Bag", "Ankle boot"]


    def __init__(self, model=None, parameters=brain.default_parameters, test=False):

        if test:
            self.test()

        if type(model) == str:
            self.model = keras.models.load_model(model)
        elif model and type(model) != keras.engine.training.Model:
            raise ValueError("If you want to use ready model - \nSpecify keras model or path to model!")

        self.mp_brain = brain.FlexClassificator(model=self.model if self.model else None)

        self.parameters = parameters

    def _activate_model(self):

        if not self.model:
            self.mp_brain.validate_set(self.train_set)
            self.mp_brain.set_class_names(self.class_names)
            self.mp_brain.init_model(parameters=self.parameters)

    def set_dataset(self, dataset_folder="", dataset_struct={}):
        if bool(dataset_folder) == bool(dataset_struct):
            raise ValueError("You need to specify one of dataset_folder or dataset_struct in Trainer class")

        self._init_dataset(dataset_folder if dataset_folder else dataset_struct)

    def _init_dataset(self, dataset):
        if type(dataset) == str:
            dataset = fs.parse_dataset_folder(dataset)

        self.train_set, self.train_labels, self.test_set, self.test_labels, self.class_names = parse_dataset_struct(dataset)


    def run(self, epochs):
        self._activate_model()

        self.mp_brain.train(self.train_set, self.train_labels, epochs) # (0.33636674284935, 0.8838000297546387)
        print(self.mp_brain.test(self.test_set, self.test_labels))


    def predict(self, set):
        print(self.mp_brain.predict(set))

    def save(self, path):
        self.mp_brain.save(path)
