from PIL import Image
from time import sleep

class Formater(object):

    def __init__(self, image_size=0):
        self.image_size = image_size

    def check_size(self):
        if self.image_size < 1:
            raise ValueError("Need to set positive image_size in Formater")

    def format_image(self, image):
        #try:
        self.check_size()

        image = image.convert(mode="L")
        #except OSError:
        #    print(" Goddam OSError....")

        image = image.resize((self.image_size,self.image_size), Image.LANCZOS)
        return image

    def format_images(self, images):

        for i in range(len(images)):
            images[i] = self.format_image(images[i])

        return images
