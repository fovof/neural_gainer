from google_images_search import GoogleImagesSearch
from PIL import Image, UnidentifiedImageError
from io import BytesIO
from requests.exceptions import ConnectionError

class Searcher(object):
    bytes_io = BytesIO()

    def __init__(self, max_results):
        self.api = self._start_api()
        self.max_results = max_results

    def _start_api(self):
        self.search_params = {
            "q":"",
            "num":10,
            "fileType":"png",
            "imgType":"photo"
        }
        #GCS api key, GSE ID
        return GoogleImagesSearch("AIzaSyAkjIMfvzVudrRt0w2Bn4xeLPOXirolo54", "09d013886652c4463")

    def find_images(self, search_text):
        self.search_params["q"] = search_text
        try:
            self.api.search(search_params=self.search_params)
        except ConnectionError:
            print("Fuck ConnectionError happened again... \n\tGlory to google_images_search\n")
            return []

        res = 0
        images = []

        while res < self.max_results:
            res += 10

            for image in self.api.results():

                self.bytes_io = BytesIO()

                self.bytes_io.seek(0)

                raw_image_data = image.get_raw_data()

                image.copy_to(self.bytes_io, raw_image_data)
                #image.copy_to(self.bytes_io)

                self.bytes_io.seek(0)

                try:
                    image = Image.open(self.bytes_io)

                    images.append(image)
                except UnidentifiedImageError:
                    print("Fuck UnidentifiedImageError\n")

            try:
                self.api.next_page()
            except ConnectionError:
                print("Fuck ConnectionError happened again... \n\tGlory to google_images_search\n")
                continue

        return images
