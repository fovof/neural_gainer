import crawler.searcher as searcher
import crawler.formater as formater
import filesystem

class Crawler(object):

    def __init__(self, categories_dict, image_folder, image_size, max_results=10):
        self.categories_dict = categories_dict
        self.image_folder = image_folder
        self.image_size = image_size
        self.max_results = max_results

    def run(self):
        s = searcher.Searcher(self.max_results)
        f = formater.Formater(self.image_size)

        dataset_struct = {}

        for category in self.categories_dict.keys():

            dataset_struct[category] = {}
            dataset_struct[category]["train"] = []
            dataset_struct[category]["test"] = []

            for search_text in self.categories_dict[category]:
                images = s.find_images(search_text)
                images = f.format_images(images)

                print("Found and formated {0} images, {1} images saved to training\n".format(int(len(images)),int(len(images) * 0.9)))
                filesystem.save_set(images[:int(len(images) * 0.9)], search_text, self.image_folder, category, "train")
                filesystem.save_set(images[int(len(images) * 0.9):], search_text, self.image_folder, category, "test")

                dataset_struct[category]["train"].extend(images[:int(len(images) * 0.9)])
                dataset_struct[category]["test"].extend(images[int(len(images) * 0.9):])

        return dataset_struct
