# TensorFlow и tf.keras
import tensorflow as tf
from tensorflow import keras

# Вспомогательные библиотеки
import numpy as np
import matplotlib.pyplot as plt

# Основа разспознавательной сети
from intellect.base import Classificator, default_parameters
from intellect.base import Classificator, default_parameters

class MultilayerPerceptron(Classificator):


    def init_model(self, class_names=[], parameters=default_parameters):

        print(self.set_shape, self.class_names, "\n\n\n\n")

        self._validate_and_set_parameters(parameters)

        layers = [keras.layers.Flatten()]#input_shape=self.set_shape)]

        for layer in self.parameters["layers"]:
            layers.append(keras.layers.Dense(layer["nodes"],activation=layer["activation"]))

        layers.append(keras.layers.Dense(len(self.class_names), activation="softmax"))

        self.model = keras.Sequential(layers)

        self.model.compile(optimizer=self.parameters["optimizer"],
              loss=self.parameters["loss"],
              metrics=["accuracy"])



    def train(self, train_set, train_labels, epochs):
        if not self.model:
            raise ValueError("Set or init model with init_model()")

        self._validate_and_set_fields(train_set, train_labels)

        self.model.fit(train_set, train_labels, epochs=epochs)


    def test(self, test_set,  test_labels):
        if self.validate_set(test_set) and self._validate_labels_num(test_labels):
            test_loss, test_acc = self.model.evaluate(test_set,  test_labels, verbose=2)
        else:
            return False, False

        return test_loss, test_acc
