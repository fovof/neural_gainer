import json
import os
from PIL import Image

def parse_parameters(filepath):

    with open(filepath, "r") as file:

        file_struct = json.load(file)
        for layer in file_struct["layers"]:
            if "kernel" in list(layer.keys()):
                layer["kernel"] = tuple(layer["kernel"])

    return file_struct

def parse_dataset_folder(folder_path):
    dataset_struct = {}

    for entry in os.listdir(folder_path):

        if os.path.isdir(os.path.join(folder_path, entry)):

            dir_path = os.path.join(folder_path, entry)
            dataset_struct[entry] = {}
            dataset_struct[entry]["train"] = []
            dataset_struct[entry]["test"] = []

            for image in os.listdir(os.path.join(dir_path, "train")):

                img_path = os.path.join(dir_path, "train", image)
                dataset_struct[entry]["train"].append(Image.open(img_path))

            for image in os.listdir(os.path.join(dir_path, "test")):

                img_path = os.path.join(dir_path, "test", image)
                dataset_struct[entry]["test"].append(Image.open(img_path))

    return dataset_struct

def save_set(images, filename, *path):
    path_dir = "/".join(path) # TODO change / for os separator
    os.makedirs(path_dir, exist_ok=True)

    img_num = 1
    for image in images:
        image.save(path_dir + "/" + filename + "_" + str(img_num), "PNG")# TODO change / for os separator
        img_num += 1


def open_json(filepath):

    with open(filepath, "r") as file:

        file_struct = json.load(file)

        if type(file_struct) == dict:

            for key in file_struct.keys():

                if type(file_struct[key]) == dict:

                    if "main" in file_struct[key].keys() \
                    and "side" in file_struct[key].keys() \
                    and type(file_struct[key]["main"]) == list \
                    and type(file_struct[key]["side"]) == list:

                        search_array = file_struct[key]["main"][:]
                        for prefix in file_struct[key]["side"]:
                            search_array.extend([" ".join([prefix, x]) for x in file_struct[key]["main"]])

                        file_struct[key] = search_array

                    else:
                        raise TypeError("File {0} is wrong format".format(filepath))

                elif type(file_struct[key]) == list:

                    continue

                else:
                    raise TypeError("File {0} is wrong format".format(filepath))
        else:
            raise TypeError("File {0} is wrong format".format(filepath))

        return file_struct
